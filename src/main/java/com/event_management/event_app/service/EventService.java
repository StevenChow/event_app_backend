package com.event_management.event_app.service;

import com.event_management.event_app.dto.EventDto;

import java.util.List;

public interface EventService {
    EventDto createEvent(EventDto eventDto);

    List<EventDto> getAllEvents();

    EventDto getEventById(int id);

    EventDto updateEvent(int id, EventDto eventDto);

    void deleteEvent(int id);
}
