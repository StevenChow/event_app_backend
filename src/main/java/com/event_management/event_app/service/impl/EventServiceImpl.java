package com.event_management.event_app.service.impl;

import com.event_management.event_app.dto.EventDto;
import com.event_management.event_app.entity.Event;
import com.event_management.event_app.mapper.EventMapper;
import com.event_management.event_app.repository.EventRepository;
import com.event_management.event_app.service.EventService;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl implements EventService {
    private EventRepository eventRepository;

    public EventServiceImpl(EventRepository eventRepository){
        this.eventRepository = eventRepository;
    }

    @Override
    public List<EventDto> getAllEvents() {
        List<Event> events = eventRepository.findAll();

        return events.stream().map((event) -> EventMapper.mapToEventDto(event))
                .collect(Collectors.toList());
    }

    @Override
    public EventDto createEvent(EventDto eventDto) {
        Event event = EventMapper.mapToEvent(eventDto);
        Event savedEvent = eventRepository.save(event);

        return EventMapper.mapToEventDto(savedEvent);
    }

    @Override
    public EventDto getEventById(int id) {
        Event event = eventRepository
                .findById(id)
                .orElseThrow(() ->
                        new RuntimeException("Event does not exist.")
                );

        return EventMapper.mapToEventDto(event);
    }

    @Override
    public EventDto updateEvent(int id, EventDto eventDto) {
        Event event = eventRepository
                .findById(id)
                .orElseThrow(() ->
                        new RuntimeException("Event does not exist.")
                );

        // Update info when found
        event.setName(eventDto.getName());
        event.setDescription(eventDto.getDescription());
        event.setLocation(eventDto.getLocation());
        event.setDate(eventDto.getDate());
        event.setTime(eventDto.getTime());
        event.setEventStatus(eventDto.getEventStatus());

        Event updatedEvent = eventRepository.save(event);

        return EventMapper.mapToEventDto(updatedEvent);
    }

    @Override
    public void deleteEvent(int id) {
        Event event = eventRepository
                .findById(id)
                .orElseThrow(() ->
                        new RuntimeException("Event does not exist.")
                );

        eventRepository.deleteById(id);
    }
}
