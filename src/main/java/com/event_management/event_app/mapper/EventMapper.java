package com.event_management.event_app.mapper;

import com.event_management.event_app.dto.EventDto;
import com.event_management.event_app.entity.Event;

public class EventMapper {

    public static Event mapToEvent(EventDto eventDto) {
        Event event = new Event(
                eventDto.getId(),
                eventDto.getName(),
                eventDto.getDescription(),
                eventDto.getLocation(),
                eventDto.getDate(),
                eventDto.getTime(),
                eventDto.getEventStatus()
        );

        return event;
    }

    public static EventDto mapToEventDto(Event event) {
        EventDto eventDto = new EventDto(
                event.getId(),
                event.getName(),
                event.getDescription(),
                event.getLocation(),
                event.getDate(),
                event.getTime(),
                event.getEventStatus()
        );

        return eventDto;
    }
}
