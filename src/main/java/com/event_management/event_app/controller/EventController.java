package com.event_management.event_app.controller;

import com.event_management.event_app.dto.EventDto;
import com.event_management.event_app.entity.Event;
import com.event_management.event_app.service.EventService;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/events")
public class EventController {

    // Dependency Injection
    private EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    // GET method
    @GetMapping
    public List<EventDto> getEvents() {
        List<EventDto> events = eventService.getAllEvents();
        return events;
    }

    // GET by ID method
    @GetMapping("/{id}")
    public EventDto getEventById(@PathVariable int id) {
        EventDto eventDto = eventService.getEventById(id);
        return eventDto;
    }

    // POST method
    @PostMapping("/create")
    public EventDto createEvent(@RequestBody EventDto eventDto) {
        EventDto response = eventService.createEvent(eventDto);
        return response;
    }

    // PUT Method
    @PutMapping("/{id}")
    public EventDto updateEvent(@PathVariable int id, @RequestBody EventDto eventDto) {
        EventDto response = eventService.updateEvent(id, eventDto);
        return response;
    }

    // Delete Method
    @DeleteMapping("/{id}")
    public String deleteEvent(@PathVariable int id) {
        eventService.deleteEvent(id);
        return "Event is deleted successfully!";
    }
}
