package com.event_management.event_app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Time;
import java.util.Date;

@Data
@AllArgsConstructor
public class EventDto {
    private int id;
    private String name;
    private String description;
    private String location;
    private Date date;
    private Time time;
    private String eventStatus;
}
